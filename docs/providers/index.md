# Providers

<figure markdown>
![Jasmine Scott-Cochran](assets/jasmine.jpeg){ height="384" width="256" loading="lazy" }
  <center>
    <figcaption markdown>
    [Jasmine Scott-Cochran](jasmine.md)<br>MS, NCC, LPC
    </figcaption>
  </center>
</figure>
