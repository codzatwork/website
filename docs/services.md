# Services

We provide out-patient psychotherapy services for individuals and couples.

All psychotherapy session appointments are scheduled for sixty (60) minutes for both new and existing clients.
Free consultations for potential clients that have yet to select our practice or a provider are fifteen (15) minutes.

We see anyone _**ages 13 and up**_. We see clients regardless of race, sexual orientation, gender identity, religion, or political leanings.
